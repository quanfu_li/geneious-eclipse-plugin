package com.geneious.plugin.grojectWiard;

import org.eclipse.jface.wizard.WizardPage;

public abstract class AbstractUIPage extends WizardPage {

	public AbstractUIPage(String pageName) {
		super(pageName);
	}
}
