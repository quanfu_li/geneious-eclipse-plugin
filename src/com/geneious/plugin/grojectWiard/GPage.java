package com.geneious.plugin.grojectWiard;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.ide.IIDEHelpContextIds;

public class GPage extends WizardPage {

	Text sdkDirText;
	List templateList;
	
	public GPage(String pageName) {
		super(pageName);
	}

	@Override
	public void createControl(Composite parent) {
		final Composite composite = new Composite(parent, SWT.NULL);
		initializeDialogUnits(parent);

		PlatformUI.getWorkbench().getHelpSystem()
				.setHelp(composite, IIDEHelpContextIds.NEW_PROJECT_WIZARD_PAGE);

		composite.setLayout(new GridLayout());
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));

		// project specification group
		Composite projectGroup = new Composite(composite, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		projectGroup.setLayout(layout);
		projectGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// new project label
		Label projectLabel = new Label(projectGroup, SWT.NONE);
		projectLabel.setText("Geneious SDK directory : ");
		projectLabel.setFont(composite.getFont());

		// new project name entry field
		sdkDirText = new Text(projectGroup, SWT.BORDER);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.widthHint = 250;
		sdkDirText.setLayoutData(data);
		sdkDirText.setFont(composite.getFont());

//		Composite tempeGroup = new Composite(composite, SWT.NONE);
//		layout = new GridLayout();
//		layout.numColumns = 2;
//		tempeGroup.setLayout(layout);
//		tempeGroup.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_CENTER));
		
		final Composite templateListLayout = new Composite(composite, SWT.NONE);
		layout = new GridLayout();
		layout.numColumns = 2;
		layout.marginTop = 20;
		layout.marginLeft = 20;
		templateListLayout.setLayout(layout);
		templateListLayout.setLayoutData(new GridData(GridData.VERTICAL_ALIGN_BEGINNING));
		
		templateList = new List(templateListLayout, SWT.BORDER | SWT.V_SCROLL);
		templateList.add("BackTranslationPlugin");
		templateList.add("ExampleDocumentViewer");
		templateList.add("ExampleGeneiousService");
		templateList.add("ExampleSequenceGraph2");
		templateList.add("ExampleWorkflowPlugin");
		templateList.add("JavadocExamplesPlugin");
		templateList.add("ExampleAlignmentOperation");
		templateList.add("ExampleFastaExporter");
		templateList.add("ExampleSequenceAnnotationGenerator");
		templateList.add("ExampleSequenceViewerStatistics");
		templateList.add("ReverseSequence");
		templateList.add("ExampleAssembler");
		templateList.add("ExampleFastaImporter");
		templateList.add("ExampleSequenceGraph");
		templateList.add("ExampleTreeViewerExtensionPlugin");
		templateList.add("HelloWorld");
		templateList.add("TextFiles");
		
		final Label templateDescription = new Label(templateListLayout, SWT.NONE);
		templateDescription.setFont(composite.getFont());
		
		templateList.addListener(SWT.Selection, new Listener() {
			
		      public void handleEvent(Event e) {
		    	  String des;
		          int[] selection = templateList.getSelectionIndices();
		          des = templateList.getItem(selection[0]);
		          templateDescription.setText("descprions for template " + des);
		          templateListLayout.layout();
		          composite.layout();
		        }
		      });
		
		setErrorMessage(null);
		setMessage(null);
		setControl(composite);
		Dialog.applyDialogFont(composite);
	}
	
	public String getSDKHome() {
		return sdkDirText.getText();
	}
	
	public String getTemplateSelected() {
		return templateList.getSelection()[0];
	}
}
