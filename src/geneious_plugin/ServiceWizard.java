package geneious_plugin;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import com.geneious.plugin.grojectWiard.AbstractComponentWizard;

public class ServiceWizard extends AbstractComponentWizard {

	public ServiceWizard() {
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		super.init(workbench, selection);
	}

	@Override
	public boolean performFinish() {
		super.performFinish();
		return true;
	}

}
