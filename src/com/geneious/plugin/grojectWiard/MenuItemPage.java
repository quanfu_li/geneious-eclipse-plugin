package com.geneious.plugin.grojectWiard;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.ide.IIDEHelpContextIds;

public class MenuItemPage extends AbstractUIPage {

	private Text nameText;
	private Text idText;
	private Text packageText;
	private Text classText;
	private Combo positionDropList;
	private Combo menuDropList;
	private Combo itemList;

	public MenuItemPage(String pageName) {
		super(pageName);
	}

	@Override
	public void createControl(Composite parent) {
		final Composite composite = new Composite(parent, SWT.NULL);
		initializeDialogUnits(parent);

		PlatformUI.getWorkbench().getHelpSystem()
				.setHelp(composite, IIDEHelpContextIds.NEW_PROJECT_WIZARD_PAGE);

		composite.setLayout(new GridLayout());
		composite.setLayoutData(new GridData(GridData.FILL_BOTH));

		nameText = 		addLableAndText("Name	", composite);
		idText = 		addLableAndText("Id		", composite);
		packageText = 	addLableAndText("Package	", composite);
		classText = 	addLableAndText("Class	", composite);

		Composite group = new Composite(composite, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 4;
		group.setLayout(layout);
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// name label
		Label anchorLable = new Label(group, SWT.NONE);
		anchorLable.setText("Position");
		anchorLable.setFont(composite.getFont());

		positionDropList = new Combo(group, SWT.DROP_DOWN | SWT.BORDER);
		positionDropList.add("before");
		positionDropList.add("after");
		
		menuDropList = new Combo(group, SWT.DROP_DOWN | SWT.BORDER);
		menuDropList.add("File");
		menuDropList.add("Edit");
		menuDropList.add("View");
		menuDropList.add("Tool");
		menuDropList.add("Sequence");
		menuDropList.add("Annotate & Predict");
		menuDropList.add("Help");
		menuDropList.add("New Menu Group");
		
		itemList = new Combo(group, SWT.DROP_DOWN | SWT.BORDER);
		itemList.add("Align/Assemble");
		itemList.add("Tree..");
		itemList.add("Fast Assembly");
		itemList.add("Primers");
		itemList.add("Cloning");
		itemList.add("Sequence Search");
		itemList.add("Add/Remove database");
		itemList.add("16s webapps");
		itemList.add("Extract ");
		
		setErrorMessage(null);
		setMessage(null);
		setControl(composite);
		Dialog.applyDialogFont(composite);

	}

	protected Text addLableAndText(String name, Composite composite) {
		// name specification group
		Composite group = new Composite(composite, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		group.setLayout(layout);
		group.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// name label
		Label projectLabel = new Label(group, SWT.NONE);
		projectLabel.setText(name);
		projectLabel.setFont(composite.getFont());

		// name entry field
		Text text = new Text(group, SWT.BORDER);
		GridData data = new GridData(GridData.FILL_HORIZONTAL);
		data.widthHint = 250;
		text.setLayoutData(data);
		text.setFont(composite.getFont());

		return text;
	}

	public String getNameText() {
		return nameText.getText();
	}

	public String getIdText() {
		return idText.getText();
	}

	public String getPackageText() {
		return packageText.getText();
	}

	public String getClassText() {
		return classText.getText();
	}

	public String getPositionDropList() {
		return positionDropList.getText();
	}

	public String getMenuDropList() {
		return menuDropList.getText();
	}

	public String getItemList() {
		return itemList.getText();
	}

}
