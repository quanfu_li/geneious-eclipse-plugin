package com.geneious.plugin.grojectWiard;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.internal.utils.FileUtil;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkingSet;
import org.eclipse.ui.IWorkingSetManager;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.ui.internal.ide.IDEWorkbenchPlugin;
import org.eclipse.ui.internal.wizards.datatransfer.ArchiveFileManipulations;
import org.eclipse.ui.internal.wizards.datatransfer.DataTransferMessages;
import org.eclipse.ui.internal.wizards.datatransfer.WizardProjectsImportPage.ProjectRecord;
import org.eclipse.ui.wizards.datatransfer.FileSystemStructureProvider;
import org.eclipse.ui.wizards.datatransfer.ImportOperation;

public class Gwizard extends Wizard implements INewWizard {

	private WizardNewProjectCreationPage pageOne;
	private GPage pageTwo;

	public Gwizard() {
		setWindowTitle("Create Geneious plugin project");
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
	}

	@Override
	public boolean performFinish() {
		String sdkHome = pageTwo.getSDKHome();
		String templateName = pageTwo.getTemplateSelected();
		String dirToCp = sdkHome + File.separator + "examples" + File.separator
				+ templateName;
		String projectName = pageOne.getProjectName();
		IPath projectPath = pageOne.getLocationPath();

		try {
			FileUtils.copyDirectory(new File(dirToCp),
					new File(projectPath.toFile(), projectName));
			modifyLaunchFile(projectPath.toFile(), templateName, projectName);
			addProjectToWorkspace(projectPath.toFile() + projectName,
					projectName);

			if (!ResourcesPlugin.getWorkspace().getRoot()
					.getProject("GeneiousFiles").exists()) {
				FileUtils.copyDirectory(new File(sdkHome + File.separator
						+ "examples" + File.separator + "GeneiousFiles"),
						new File(projectPath.toFile(), "GeneiousFiles"));
				addProjectToWorkspace(projectPath.toFile() + "GeneiousFiles",
						"GeneiousFiles");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	public static void modifyLaunchFile(File file, String templateName,
			String projectName) {
		File dir = new File(file, projectName);
		File[] listFiles = dir.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".launch");
			}
		});

		for (File f : listFiles) {
			replaceTextInFile(f,
					"<listEntry value=\"/" + templateName + "\"/>",
					"<listEntry value=\"/" + projectName + "\"/>");

			replaceTextInFile(f, "&quot;" + templateName + "&quot;", "&quot;"
					+ projectName + "&quot;");
			
			replaceTextInFile(f, "\"" + templateName + "\"", "\"" + projectName + "\"");

			String desName = f.getName().replaceAll(templateName, projectName);
			f.renameTo(new File(f.getParent(), desName));

		}
	}

	public static void replaceTextInFile(File f, String string, String string2) {
		BufferedWriter writer = null;

		try {
			StringBuilder sb = new StringBuilder();
			BufferedReader reader = new BufferedReader(new FileReader(f));
			String tmp;
			while ((tmp = reader.readLine()) != null)
				sb.append(tmp.replaceAll(string, string2)).append("\n");

			reader.close();

			writer = new BufferedWriter(new FileWriter(f, false));
			writer.append(sb.toString());
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void addProjectToWorkspace(String path, String projectName) {
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IProject project = workspace.getRoot().getProject(projectName);

		if (project.exists()) {
			return;
		}

		final IProjectDescription newProjectDescription = workspace
				.newProjectDescription(projectName);

		// import from file system
		WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
			protected void execute(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				try {
					monitor.beginTask("", 1); //$NON-NLS-1$
					try {
						project.create(newProjectDescription,
								new SubProgressMonitor(monitor, 30));
						project.open(IResource.BACKGROUND_REFRESH,
								new SubProgressMonitor(monitor, 70));
					} catch (CoreException e) {
						throw new InvocationTargetException(e);
					} finally {
						monitor.done();
					}
				} finally {
					monitor.done();
				}
			}
		};

		// run the new project creation operation
		try {
			getContainer().run(true, true, op);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addProjectToWorkplace() {
		String projectName = pageOne.getProjectName();
		final IWorkspace workspace = ResourcesPlugin.getWorkspace();
		final IProject project = workspace.getRoot().getProject(projectName);
		final IProjectDescription newProjectDescription = workspace
				.newProjectDescription(projectName);

		// import from file system
		File importSource = null;

		WorkspaceModifyOperation op = new WorkspaceModifyOperation() {
			protected void execute(IProgressMonitor monitor)
					throws InvocationTargetException, InterruptedException {
				try {
					monitor.beginTask("", 1); //$NON-NLS-1$
					try {
						project.create(newProjectDescription,
								new SubProgressMonitor(monitor, 30));
						project.open(IResource.BACKGROUND_REFRESH,
								new SubProgressMonitor(monitor, 70));
					} catch (CoreException e) {
						throw new InvocationTargetException(e);
					} finally {
						monitor.done();
					}
				} finally {
					monitor.done();
				}
			}
		};

		// run the new project creation operation
		try {
			getContainer().run(true, true, op);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addPages() {
		super.addPages();

		pageOne = new WizardNewProjectCreationPage(
				"Create Geneious plugin project");
		pageOne.setTitle("Create Geneious plugin");
		pageOne.setDescription("Create plugin to develop Geneious");
		addPage(pageOne);

		pageTwo = new GPage("Create Geneious plugin project");
		pageTwo.setTitle("Create Geneious plugin");
		pageTwo.setDescription("Create plugin to develop Geneious");
		addPage(pageTwo);
	}

	public static void main(String[] args) {
		replaceTextInFile(new File(
				"/Users/frank/tmp/HelloWorld (64 bit).launch"),
				"<listEntry value=\"/HelloWorld\"/>",
				"<listEntry value=\"/t1\"/>");
	}
}
