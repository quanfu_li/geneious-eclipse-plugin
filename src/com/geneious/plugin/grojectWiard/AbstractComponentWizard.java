package com.geneious.plugin.grojectWiard;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;

public class AbstractComponentWizard extends Wizard implements INewWizard {

	private AbstractComponentPage pageOne;

	public AbstractComponentWizard() {
		setWindowTitle("Create Geneious Component");
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
	}

	@Override
	public boolean performFinish() {
		return true;
	}

	@Override
	public void addPages() {
		super.addPages();
		pageOne = new AbstractComponentPage("Create Geneious plugin project");
		pageOne.setTitle("Create Geneious Component");
		addPage(pageOne);
	}
}
