package geneious_plugin;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ide.undo.CreateFileOperation;
import org.eclipse.ui.ide.undo.WorkspaceUndoUtil;
import org.eclipse.ui.internal.ide.IDEWorkbenchMessages;
import org.eclipse.ui.internal.ide.IDEWorkbenchPlugin;

import com.geneious.plugin.grojectWiard.AbstractUIWizard;
import com.geneious.plugin.grojectWiard.MenuItemPage;

public class MenuItemWizard extends AbstractUIWizard {

	private MenuItemPage page;
	
	public MenuItemWizard() {
	}

	@Override
	public void addPages() {
		page = new MenuItemPage("Create Menu item for Geneious");
		addPage(page);
	}
	
	@Override
	public boolean performFinish() {
		String name = page.getNameText();
		String idText = page.getIdText();
		String packageText = page.getPackageText();
		String classText = page.getClassText();
		String positionDropList = page.getPositionDropList();
		String menuDropList = page.getMenuDropList();
		String itemList = page.getItemList();
		
		URL tmp = getClass().getClassLoader().getResource("geneious_plugin/MenuTemplate.template");
		StringBuilder sb = new StringBuilder();
		try {
			InputStream openStream = tmp.openStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(openStream));
			String t;
			
			while ((t = reader.readLine()) != null) {
				sb.append(t).append("\n");
			}
			
			reader.close();
			
			//new MenuTemplate("#name#", "#id#", "#package#", "#className#", "#position#", "#menu#", "#item#");
			String result = sb.toString().replaceAll("#name#", name)
							.replaceAll("#id#", idText)
							.replaceAll("#package#", packageText)
							.replaceAll("#className#", classText)
							.replaceAll("#position#", positionDropList)
							.replaceAll("#menu#", menuDropList)
							.replaceAll("#item#", itemList)
							.replaceAll("#Class#", classText)
							.replaceAll("#packageName#", packageText);
			
			IStructuredSelection selected = getSelection();
			IProjectNature projectNature = (IProjectNature) selected.getFirstElement();
			IProject project = projectNature.getProject();
			IPath path = project.getFullPath().append("src/" + packageText.replaceAll("\\.", "/"));
			File file = path.append(classText).toFile();
			path.toFile().mkdirs();
//			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
//			writer.append(result);
			
			final IFile newFileHandle = IDEWorkbenchPlugin.getPluginWorkspace().getRoot().getFile(path.append(classText + ".java"));
			final InputStream initialContents = new ByteArrayInputStream(result.getBytes());

			IRunnableWithProgress op = new IRunnableWithProgress() {
				public void run(IProgressMonitor monitor) {
					CreateFileOperation op = new CreateFileOperation(newFileHandle,
							null, initialContents,
							IDEWorkbenchMessages.WizardNewFileCreationPage_title);
					try {
						// see bug https://bugs.eclipse.org/bugs/show_bug.cgi?id=219901
						// directly execute the operation so that the undo state is
						// not preserved.  Making this undoable resulted in too many 
						// accidental file deletions.
						op.execute(monitor, WorkspaceUndoUtil
								.getUIInfoAdapter(getShell()));
					} catch (final ExecutionException e) {
						e.printStackTrace();
					}
				}
			};
			try {
				getContainer().run(true, true, op);
			} catch (Exception e) {
				e.printStackTrace();
			}			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println(MenuItemWizard.class.getResource("MenuTemplate.template"));
	}
}
